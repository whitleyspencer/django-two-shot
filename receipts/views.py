from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import NewReceiptForm, NewCategoryForm, NewAccountForm


# Create your views here.


@login_required
def home(request):
    receipt_list = Receipt.objects.filter(purchaser=request.user)
    context = {"receipt_list": receipt_list}

    return render(request, "receipts/home.html", context)


@login_required
def category_list(request):
    expense_category_list = ExpenseCategory.objects.filter(owner=request.user)
    context = {"expense_categories": expense_category_list}
    return render(request, "receipts/categories.html", context)


@login_required
def account_list(request):
    accounts = Account.objects.filter(owner=request.user)
    context = {
        "accounts": accounts,
    }
    return render(request, "receipts/accounts_list.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = NewReceiptForm(request.POST)
        if form.is_valid():
            # the completed form is saved in the variable new_receipt
            # form.save(commit= False)
            #  means the instance new_receipt is created but not saved
            new_receipt = form.save(False)

            # modify new_receipt istance to have purchaser set to current user
            new_receipt.purchaser = request.user

            # now actually save the new instance
            new_receipt.save()
            return redirect("home")
    else:
        form = NewReceiptForm()
        # only expense_categories associated with current user will
        #   appear in dropdown menu on form
        form.fields["category"].queryset = ExpenseCategory.objects.filter(
            owner=request.user
        )
        # only accounts associated with current user will appear in
        #   dropdown menu on form
        form.fields["account"].queryset = Account.objects.filter(
            owner=request.user
        )
        context = {
            "form": form,
        }
        return render(request, "receipts/create_receipt.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = NewCategoryForm(request.POST)
        if form.is_valid():
            new_category = form.save(False)
            new_category.owner = request.user
            new_category.save()
            return redirect("category_list")
    else:
        form = NewCategoryForm()
        context = {"form": form}
        return render(request, "receipts/create_category.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = NewAccountForm(request.POST)
        if form.is_valid():
            new_account = form.save(False)
            new_account.owner = request.user
            new_account.save()
            return redirect(account_list)
    else:
        form = NewAccountForm()
        context = {"form": form}
        return render(request, "receipts/create_account.html", context)
